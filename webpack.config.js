const webpack = require('webpack')
const path = require('path')

const wplib = [
    'blocks',
    'components',
    'date',
    'editor',
    'element',
    'i18n',
    'utils',
    'data',
];

const MiniCssExtractPlugin = require("mini-css-extract-plugin");


module.exports = {
    entry: "./src/start.js",
    output: {
        path: '/Bitnami/wordpress-5.3.2-3/apps/wordpress/htdocs/wp-content/plugins/audioburst/p'
		, //__dirname + "/dst",
        filename: "ext_rc_components.js",
        //libraryTarget: "umd",
        library: ['wp', '[name]'],
        libraryTarget: 'window',
    },
    module: {
        rules: [
            // perform js babelization on all .js files
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ['babel-preset-env']
                    }
                }
            },
            // compile all .scss files to plain old css
            {
                test: /\.(sass|scss)$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.less$/i,
                use: ['style-loader', 'css-loader','less-loader'],
            },
        ]
    },
    plugins: [
        // extract css into dedicated file
        new MiniCssExtractPlugin({
            filename: './dst/main.min.css'
        })
    ],
    externals: wplib.reduce((externals, lib) => {
        externals[`wp.${lib}`] = {
            window: ['wp', lib],
        };

        return externals;
    }, {
        'react': 'React',
        'react-dom': 'ReactDOM',
    }),
    /*
   	externals: {
        'react-dom':'ReactDOM',
	    'react': 'React'
	},*/
  //  mode: 'none'
}
